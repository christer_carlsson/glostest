﻿namespace GlosTest
{
    public class Glosa
    {
        //Det svenska ordet i glosan
        public string SwedishWord { get; set; }
        //Det engelska ordet i glosan
        public string EnglishWord { get; set; }
    }
}