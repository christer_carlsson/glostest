﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GlosTest
{
    public partial class GlosTestForm : Form
    {
        //Lista med glosor
        private List<Glosa> _glosor;
        //Lista med felaktiga glosor
        private List<Glosa> _wrongList;
        //Den glosa som är aktuell att testa
        private int _currentGlosa;
        //Antal rätta svar 
        private int _rights;
        //Antal felaktiga svar
        private int _wrongs;

        /// <summary>
        /// Konsturktor för GlosTestForm
        /// </summary>
        public GlosTestForm()
        {
            InitializeComponent();
            _glosor = new List<Glosa>();
            _wrongList = new List<Glosa>();
            _currentGlosa = 0;
        }

        /// <summary>
        /// Klick för lägga till en glosa
        /// </summary>
        /// <param name="sender">Knappen som skapar eventet</param>
        /// <param name="e">eventuella parameterar</param>
        private void btnInput_Click(object sender, EventArgs e)
        {
            //Om båda textboxarna har ord lägg till en glosa.
            if (!(string.IsNullOrEmpty(tbxInputEng.Text)
                || string.IsNullOrEmpty(tbxInputSwe.Text)))
            {
                Glosa glosa = new Glosa();
                glosa.EnglishWord = tbxInputEng.Text;
                glosa.SwedishWord = tbxInputSwe.Text;
                tbxInputSwe.Text = tbxInputEng.Text = "";
                _glosor.Add(glosa);
                btnStart.Enabled = true;
            }
            else //Annars tala om för användaren att mata in text i båda textboxarna.
            {
                MessageBox.Show("Du måste mata in två ord.",
                    "Felaktig inmatning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        /// <summary>
        /// Klick för starta glostest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            //Avaktivera lägg till groupbox och startknapp 
            gbxAdd.Enabled = false;
            btnStart.Enabled = false;
            //Aktivera test groupboxen
            gbxTest.Enabled = true;
            //Nollställ rätta och felaktiga svar
            _rights = _wrongs = 0;
            //Starta test
            TestGlosa();
        }

        /// <summary>
        /// Testa den aktuella glosan
        /// </summary>
        private void TestGlosa()
        {
            //Finns det fortfarande glosor att testa
            if (_currentGlosa < _glosor.Count())
            {
                //Skriv in det svenska ordet.
                tbxTestSwe.Text = _glosor.ElementAt(_currentGlosa).SwedishWord;
            }
            else // är det slut på glosor
            {
                //Töm textboxarna
                tbxTestSwe.Text = "";
                tbxTestEng.Text = "";
                //Avaktivera test groupboxen
                gbxTest.Enabled = false;
                //Aktivera Nya glosor knappen
                btnNew.Enabled = true;
                //Skriv ut resultatet
                PrintResult();
            }
        }

        /// <summary>
        /// Skriv ut resultatet
        /// </summary>
        private void PrintResult()
        {
            tbxResult.Text += $"Resultat: {_rights}/{_rights + _wrongs} \r\n";
            tbxResult.Text += $"\tFelaktiga svar:\r\n";
            foreach (var glosa in _wrongList)
            {
                tbxResult.Text += $"\t\t{glosa.SwedishWord} : {glosa.EnglishWord}\r\n";
            }
        }

        /// <summary>
        /// Klick för svarknappen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnswer_Click(object sender, EventArgs e)
        {
            //Om svar textboxen inte är tom
            if (!string.IsNullOrEmpty(tbxTestEng.Text))
            {
                var currentGlosa = _glosor.ElementAt(_currentGlosa);
                //Om svaret är rätt
                if (currentGlosa.EnglishWord.ToLower() == tbxTestEng.Text.ToLower())
                {
                    _rights++;
                }
                //Annars är svaret fel
                else
                {
                    _wrongList.Add(currentGlosa);
                    _wrongs++;
                }

                //Öka till nästa glosa
                _currentGlosa++;
                //Töm svartextboxen
                tbxTestEng.Text = tbxTestSwe.Text = "";
                //Testa nästa glosa
                TestGlosa();
            }
            //Annars är text textboxen tom
            else
            {
                //Tala om för användare att svaret var tomt.
                MessageBox.Show("Du måste mata in ett svar.",
                    "Tomt svar",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
        }

        /// <summary>
        /// Klick för nytt glostest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            //Töm gloslistan
            _glosor.Clear();
            _wrongList.Clear();
            //Nollställ antal rätt, fel och sätt den första glosan som den aktuella
            _rights = _wrongs = _currentGlosa = 0;
            //Avaktivera ny glosa knappen
            btnNew.Enabled = false;
            //Aktivera Lägg till glosa groupboxen
            gbxAdd.Enabled = true;
        }
    }
}
