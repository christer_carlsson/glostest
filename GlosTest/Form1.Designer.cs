﻿namespace GlosTest
{
    partial class GlosTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxAdd = new System.Windows.Forms.GroupBox();
            this.btnInput = new System.Windows.Forms.Button();
            this.tbxInputEng = new System.Windows.Forms.TextBox();
            this.tbxInputSwe = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxTest = new System.Windows.Forms.GroupBox();
            this.btnAnswer = new System.Windows.Forms.Button();
            this.tbxTestEng = new System.Windows.Forms.TextBox();
            this.tbxTestSwe = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxResult = new System.Windows.Forms.TextBox();
            this.gbxAdd.SuspendLayout();
            this.gbxTest.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxAdd
            // 
            this.gbxAdd.Controls.Add(this.btnInput);
            this.gbxAdd.Controls.Add(this.tbxInputEng);
            this.gbxAdd.Controls.Add(this.tbxInputSwe);
            this.gbxAdd.Controls.Add(this.label2);
            this.gbxAdd.Controls.Add(this.label1);
            this.gbxAdd.Location = new System.Drawing.Point(13, 13);
            this.gbxAdd.Name = "gbxAdd";
            this.gbxAdd.Size = new System.Drawing.Size(290, 118);
            this.gbxAdd.TabIndex = 0;
            this.gbxAdd.TabStop = false;
            this.gbxAdd.Text = "Lägg till glosa";
            // 
            // btnInput
            // 
            this.btnInput.Location = new System.Drawing.Point(200, 72);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(75, 23);
            this.btnInput.TabIndex = 2;
            this.btnInput.Text = "Lägg till";
            this.btnInput.UseVisualStyleBackColor = true;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // tbxInputEng
            // 
            this.tbxInputEng.Location = new System.Drawing.Point(87, 46);
            this.tbxInputEng.Name = "tbxInputEng";
            this.tbxInputEng.Size = new System.Drawing.Size(188, 20);
            this.tbxInputEng.TabIndex = 3;
            // 
            // tbxInputSwe
            // 
            this.tbxInputSwe.Location = new System.Drawing.Point(87, 20);
            this.tbxInputSwe.Name = "tbxInputSwe";
            this.tbxInputSwe.Size = new System.Drawing.Size(188, 20);
            this.tbxInputSwe.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Engelskt ord";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Svenskt ord";
            // 
            // gbxTest
            // 
            this.gbxTest.Controls.Add(this.btnAnswer);
            this.gbxTest.Controls.Add(this.tbxTestEng);
            this.gbxTest.Controls.Add(this.tbxTestSwe);
            this.gbxTest.Controls.Add(this.label3);
            this.gbxTest.Controls.Add(this.label4);
            this.gbxTest.Enabled = false;
            this.gbxTest.Location = new System.Drawing.Point(12, 137);
            this.gbxTest.Name = "gbxTest";
            this.gbxTest.Size = new System.Drawing.Size(290, 106);
            this.gbxTest.TabIndex = 1;
            this.gbxTest.TabStop = false;
            this.gbxTest.Text = "Glostest";
            // 
            // btnAnswer
            // 
            this.btnAnswer.Location = new System.Drawing.Point(200, 71);
            this.btnAnswer.Name = "btnAnswer";
            this.btnAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnAnswer.TabIndex = 6;
            this.btnAnswer.Text = "Svar";
            this.btnAnswer.UseVisualStyleBackColor = true;
            this.btnAnswer.Click += new System.EventHandler(this.btnAnswer_Click);
            // 
            // tbxTestEng
            // 
            this.tbxTestEng.Location = new System.Drawing.Point(88, 45);
            this.tbxTestEng.Name = "tbxTestEng";
            this.tbxTestEng.Size = new System.Drawing.Size(188, 20);
            this.tbxTestEng.TabIndex = 8;
            // 
            // tbxTestSwe
            // 
            this.tbxTestSwe.Location = new System.Drawing.Point(88, 19);
            this.tbxTestSwe.Name = "tbxTestSwe";
            this.tbxTestSwe.ReadOnly = true;
            this.tbxTestSwe.Size = new System.Drawing.Size(188, 20);
            this.tbxTestSwe.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Engelskt ord";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Svenskt ord";
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(31, 249);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(98, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Starta test";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnNew
            // 
            this.btnNew.Enabled = false;
            this.btnNew.Location = new System.Drawing.Point(179, 249);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(124, 23);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "Nytt Glostest";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxResult);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(344, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 259);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultat";
            // 
            // tbxResult
            // 
            this.tbxResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxResult.Location = new System.Drawing.Point(23, 20);
            this.tbxResult.Multiline = true;
            this.tbxResult.Name = "tbxResult";
            this.tbxResult.ReadOnly = true;
            this.tbxResult.Size = new System.Drawing.Size(243, 210);
            this.tbxResult.TabIndex = 0;
            // 
            // GlosTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 303);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.gbxTest);
            this.Controls.Add(this.gbxAdd);
            this.Name = "GlosTestForm";
            this.Text = "Glostest";
            this.gbxAdd.ResumeLayout(false);
            this.gbxAdd.PerformLayout();
            this.gbxTest.ResumeLayout(false);
            this.gbxTest.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxAdd;
        private System.Windows.Forms.Button btnInput;
        private System.Windows.Forms.TextBox tbxInputEng;
        private System.Windows.Forms.TextBox tbxInputSwe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxTest;
        private System.Windows.Forms.Button btnAnswer;
        private System.Windows.Forms.TextBox tbxTestEng;
        private System.Windows.Forms.TextBox tbxTestSwe;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxResult;
    }
}

